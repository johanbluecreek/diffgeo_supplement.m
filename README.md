# diffgeo_supplement.m
## Supplementary definitions to M. Headricks diffgeo.m package

This package is a small supplement to [M. Headricks](http://people.brandeis.edu/~headrick/Mathematica/) diffgeo.m Mathematica package that defines vielbeins and spin connections of various index structures for a diagonal metric.

## Usage

Follow instructions on how to use diffgeo.m, and after it has been correctly initialised run `diffgeo_supplement.m` in a notebook as

 ```<< /full/path/to/diffgeo_supplement.m```

See package file for more information.

