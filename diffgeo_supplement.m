(* ::Package:: *)

(*

diffgeo_supplement.m

A Mathematica package to supplement diffgeo.m by M. Headrick, by J. Bl\[ARing]b\[ADoubleDot]ck.

Package intruduces,

 vielbeins
 spin connection

with various index structures.

*)



(*
Bugs/not yet implemented features/limitations/dependencies.

 General:
  * Only diagonal metrics.

*)



(*
TODO.

 General:
  * Speed up the computation of the spin connection.
	* Clean up code.
  * Add usage descriptions.
  * Non-diagonal metrics?
  * This file needs a big clean up! Make one cell which has the spin con. expressions
	for non-diagonal metrics, and print a warning when those are used -- since vielbeins
	are wrong. Then have a cell for the diagonal metrics. Also extend the functions to
	work in a similar way. This way the code can easily be extended to non-diagonal metrics
	when I descide to implement it.
  * Write a documentation notebook.

 Functions:
  * Speed them up.
  * Add usage descriptions to them.
*)



(*Usage descriptions*)




(*Initial checks*)

(*Check if lists are defined, print warnings.*)

If[
	ToString[Definition[verbose]]===ToString[Null],
		Print["W: The 'verbose' variable has not been set. Turning on verbose messages."];
		verbose = True;
]

If[
	ToString[Definition[metric]]===ToString[Null],
		Print["E: The matrix, 'metric' has not been defined.
 Please define this list before running the package."],
		If[verbose,Print["I: 'metric' has been defined."]]
]

If[
	Not[MatrixQ[metric]],
		Print["E: 'metric' should be a matrix. Redefine it and rerun package."],
		If[verbose,Print["I: 'metric' is a matrix."]]
]

diagonalQ[matIn_]:=Module[{mat=matIn,tmp},
	tmp=DiagonalMatrix[Table[mat[[ii,ii]],{ii,Length[mat]}]];
	If[tmp===mat
		True,
		False
	]
];

If[
	Not[diagonalQ[metric]],
		Print["E: 'metric' is not diagonal. This package only support diagonal metrics presently."],
		If[verbose,Print["I: 'metric' is diagonal."]]
]

If[
	ToString[Definition[inverse]]===ToString[Null],
		Print["E: The matrix, 'inverse' has not been defined.
 This is most likely due to you not loading M. Headrick's package 'diffgeo.m' before loading this package. Failure to comply will result in faulty output."],
		If[verbose,Print["I: 'inverse' has been defined."]]
]

(*
 To fix:
  * Why do I need ToString over Null and Definition[] in the above?
*)


(*Common functions*)




(*Flat metric*)
eta := Block[{ret},
	ret = DiagonalMatrix[Table[1,{Length[metric]}]];
	ret[[1,1]] = metricsign ret[[1,1]];
	ret
];

etaInv:=Inverse[eta];



(*Vielbeins*)
If[verbose,Print["I: If you want the vielbeins to not have square root expressions, add appropriate assumptions to '$Assumptions'"]];

(*Make the vielbein and some raised and lowered versions, all are namned by UD (up)(down) cf (curved)(flat), in that order*)

(*Vielbein tensor with first index down curved, second/last index up flat.*)
tmpVar=metric;
tmpVar[[1,1]]=tmpVar[[1,1]]metricsign;
vielbeinDcUf=Sqrt[tmpVar]//Simplify
Clear[tmpVar]

vielbeinUcDf=Inverse[vielbeinDcUf];
vielbeinUcUf=Table[Sum[vielbeinUcDf[[mm,mmHat]]etaInv[[mmHat,nnHat]],{mmHat,coord}],{mm,coord},{nnHat,coord}];
vielbeinDcDf=Table[Sum[vielbeinDcUf[[mm,mmHat]]eta[[mmHat,nnHat]],{mmHat,coord}],{mm,coord},{nnHat,coord}];

If[verbose,Print["I: Vielbeins computed."]];



(*Since the vielbein calculation so far only supports diagonal metrics, we will use the following formula*)
(*Taken from James Liu's JTL lecture.*)

spinConDcDcDc = Table[
		1/2(D[metric[[mm,nn]],ll]-D[metric[[mm,ll]],nn])
	,{mm,coord},{nn,coord},{ll,coord}];

spinConDcDcDf = Table[
		Sum[
			spinConDcDcDc[[mm,nn,ll]]vielbeinUcDf[[ll,aaHat]]
		,{ll,coord}]
	,{mm,coord},{nn,coord},{aaHat,coord}];

spinConDcDfDf = Table[
		Sum[
			spinConDcDcDf[[mm,nn,bbHat]]vielbeinUcDf[[nn,aaHat]]
		,{nn,coord}]
	,{mm,coord},{aaHat,coord},{bbHat,coord}];

spinConDfDfDf = Table[
		Sum[
			spinConDcDfDf[[mm,bbHat,ccHat]]vielbeinUcDf[[mm,aaHat]]
		,{mm,coord}]
	,{aaHat,coord},{bbHat,coord},{ccHat,coord}];

spinConDcDfUf = Table[
		Sum[
			spinConDcDfDf[[mm,bbHat,ddHat]]etaInv[[ddHat,ccHat]]
		,{ddHat,coord}]
	,{mm,coord},{bbHat,coord},{ccHat,coord}];

spinConDcUfUf = Table[
		Sum[
			spinConDcDfUf[[mm,ddHat,ccHat]]etaInv[[ddHat,bbHat]]
		,{ddHat,coord}]
	,{mm,coord},{bbHat,coord},{ccHat,coord}];

If[verbose,Print["I: All spin connections computed."]];


(*Functions*)

(*These are functions to get the above objects from a metric that is not a global variable.*)

(*Note that these are not fast functions -- be careful not to use on too complicated metrics.*)

(*get the flat metric*)
flatM[metIn_,msignIn_] := Module[{met=metIn,msign=msignIn,flat},
	flat = DiagonalMatrix[Table[1,{Length[met]}]];
	flat[[1,1]] = flat[[1,1]] msign;
	flat
];

(*vielbein functions*)
getVDcUf[metIn_,msignIn_] := Module[{met=metIn,msign=msignIn,tmpVar},
	tmpVar=met;
	tmpVar[[1,1]]=tmpVar[[1,1]]msign;
	Sqrt[tmpVar]//Simplify
];

getVUcDf[metIn_,msignIn_] := Module[{met=metIn,msign=msignIn},
	Inverse[getVDcUf[met,msign]]
];

(*spin con. functions*)

(*Two copies of this one since metricsign is not needed, but we might want the same argument structure for all getSC*)
getSCDcDcDc[metIn_,msignIn_,coorIn_]:=Module[{met=metIn,coor=coorIn},
	Table[
		1/2(D[met[[mm,nn]],coor[[ll]]]-D[met[[mm,ll]],coor[[nn]]])
	,{mm,Length[coor]},{nn,Length[coor]},{ll,Length[coor]}]
];
getSCDcDcDc[metIn_,coorIn_]:=Module[{met=metIn,coor=coorIn},
	Table[
		1/2(D[met[[mm,nn]],coor[[ll]]]-D[met[[mm,ll]],coor[[nn]]])
	,{mm,Length[coor]},{nn,Length[coor]},{ll,Length[coor]}]
];

getSCDcDcDf[metIn_,msignIn_,coorIn_]:=Module[{met=metIn,coor=coorIn,scDcDcDc=getSCDcDcDc[metIn,msignIn,coorIn],vUcDf=getVUcDf[metIn,msignIn]},
	Table[
		Sum[
			scDcDcDc[[mm,nn,ll]]vUcDf[[ll,aaHat]]
		,{ll,Length[coor]}]
	,{mm,Length[coor]},{nn,Length[coor]},{aaHat,Length[coor]}]
];

getSCDcDfDf[metIn_,msignIn_,coorIn_]:=Module[{met=metIn,coor=coorIn,scDcDcDf=getSCDcDcDf[metIn,msignIn,coorIn],vUcDf=getVUcDf[metIn,msignIn]},
	Table[
		Sum[
			scDcDcDf[[mm,nn,bbHat]]vUcDf[[nn,aaHat]]
		,{nn,Length[coor]}]
	,{mm,Length[coor]},{aaHat,Length[coor]},{bbHat,Length[coor]}]
];

getSCDfDfDf[metIn_,msignIn_,coorIn_]:=Module[{met=metIn,coor=coorIn,scDcDfDf=getSCDcDfDf[metIn,msignIn,coorIn],vUcDf=getVUcDf[metIn,msignIn]},
	Table[
		Sum[
			scDcDfDf[[mm,bbHat,ccHat]]vUcDf[[mm,aaHat]]
		,{mm,Length[coor]}]
	,{aaHat,Length[coor]},{bbHat,Length[coor]},{ccHat,Length[coor]}]
];

getSCDcDfUf[metIn_,msignIn_,coorIn_]:=Module[{met=metIn,coor=coorIn,scDcDfDf=getSCDcDfDf[metIn,msignIn,coorIn],flatInv=flatM[metIn,msignIn]},
	Table[
		Sum[
			scDcDfDf[[mm,bbHat,ddHat]]flatInv[[ddHat,ccHat]]
		,{ddHat,Length[coor]}]
	,{mm,Length[coor]},{bbHat,Length[coor]},{ccHat,Length[coor]}]
];

getSCDcUfUf[metIn_,msignIn_,coorIn_]:=Module[{met=metIn,coor=coorIn,scDcDfUf=getSCDcDfUf[metIn,msignIn,coorIn],flatInv=flatM[metIn,msignIn]},
	Table[
		Sum[
			scDcDfUf[[mm,ddHat,ccHat]]flatInv[[ddHat,bbHat]]
		,{ddHat,Length[coor]}]
	,{mm,Length[coor]},{bbHat,Length[coor]},{ccHat,Length[coor]}]
];



(*/spin con. functions*)
